<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Treatment;
use DB;

class TreatmentController extends Controller
{
    /**
     * Show All Treatments
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {   
        $objTreatments = Treatment::with('subservice.service')->get();
        foreach($objTreatments as $t) {
            $treatments[] = [
                "id" => $t->id,
                "treatment" => $t->name,
                "service" => $t->subservice->service->name,
                "subservice" => $t->subservice->name,
                "pvp" => $t->pvp,
                "created_at" => $t->created_at,
                "actions" => "Ver"        // data for HUDDatatables react library
            ];
        }

        return $treatments;
    }

    /**
     * Show the detail treatment.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($treatment_id)
    {
        $treatment = Treatment::find($treatment_id);
        $treatment->subservice->service;

        return $treatment;
    }
}