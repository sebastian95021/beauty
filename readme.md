##  how to run the project locally.

##  Requirements
* PHP 7.1
* Composer
* postgreSQL
* GIT

##  Configuration and run
* the first step you should do is run composer install command for install dependencies.
* Create database with the name "beauty".
* The next thing you should do is run the migrations and seeds included in this template.
* right now you should see the initial screen of the project

##  Notes
* The CSS library used in this project is Material UI for React https://material-ui.com
* the table library used in this project was created by me
* The project run on react compiled build production (created with npm run build command)
* URL react fronted project:  https://gitlab.com/sebastian95021/fronted-beauty/-/tree/master
